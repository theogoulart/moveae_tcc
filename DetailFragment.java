package br.com.sblack.myapp;

import android.content.ContentUris;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import br.com.sblack.myapp.activities.InviteesActivity;
import br.com.sblack.myapp.activities.LoginActivity;
import br.com.sblack.myapp.activities.OffersActivity;
import br.com.sblack.myapp.activities.ProfileActivity;
import br.com.sblack.myapp.data.EventContract;
import br.com.sblack.myapp.network.NetworkHelper;
import br.com.sblack.myapp.network.NetworkReceiver;
import br.com.sblack.myapp.network.PostHelper;
import br.com.sblack.myapp.network.RestHelper;

public class DetailFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final String LOG_TAG = DetailFragment.class.getSimpleName();
    private static final int DETAIL_LOADER = 0;

    private static final String[] EVENT_COLUMNS = {
            EventContract.EventEntry.TABLE_NAME+"."+ EventContract.EventEntry._ID,
            EventContract.EventEntry.TABLE_NAME+"."+ EventContract.EventEntry.COLUMN_EVENT_ID,
            EventContract.EventEntry.COLUMN_EVENT_NAME,
            EventContract.EventEntry.COLUMN_EVENT_INTRO,
            EventContract.EventEntry.COLUMN_EVENT_DESC,
            EventContract.EventEntry.COLUMN_EVENT_DATE,
            EventContract.EventEntry.COLUMN_USER_KEY,
            EventContract.EventEntry.COLUMN_CAT_KEY,
            EventContract.EventEntry.COLUMN_EVENT_ICON,
            EventContract.EventEntry.COLUMN_EVENT_LAT,
            EventContract.EventEntry.COLUMN_EVENT_LONG,
            EventContract.EventEntry.COLUMN_EVENT_MODEL
    };

    // indices for getting the columns in the event table
    static final int COL_ID = 0;
    static final int COL_EVENT_ID = 1;
    static final int COL_EVENT_NAME = 2;
    static final int COL_EVENT_INTRO = 3;
    static final int COL_EVENT_DESC = 4;
    static final int COL_EVENT_DATE = 5;
    static final int COL_USER_KEY = 6;
    static final int COL_CAT_KEY = 7;
    static final int COL_EVENT_ICON = 8;
    static final int COL_EVENT_LAT = 9;
    static final int COL_EVENT_LONG = 10;
    static final int COL_EVENT_MODEL = 11;

    TextView mNameView;
    TextView mDetailView;
    TextView mDateView;
    TextView mTimeView;

    private NetworkReceiver mNetworkReceiver;
    private IntentFilter mNetworkFilter;
    private boolean mConnection;

    Uri mEventUri;
    private String mLongitude;
    private String mLatitude;
    private String mEventName;

    public DetailFragment() {
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_detail, container, false);

        // POPULATE VIEWS

        mNameView = (TextView) rootView.findViewById(R.id.detail_name_textview);
        mDetailView = (TextView)rootView.findViewById(R.id.detail_desc_textview);
        mDateView = (TextView) rootView.findViewById(R.id.detail_date_textview);
        mTimeView = (TextView) rootView.findViewById(R.id.detail_time_textview);

        // GET URIS

        Intent intent = getActivity().getIntent();
        if(intent.getData() != null){
            mEventUri = intent.getData();
        }

        if(intent.hasExtra(EventFragment.CONNECTION_EXTRA)){
            mConnection = intent.getBooleanExtra(EventFragment.CONNECTION_EXTRA, false);
        }

        // LISTENERS

        Button btnInvitees = (Button) rootView.findViewById(R.id.detail_view_invitees_button);
        btnInvitees.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View view) {
                listInvitees();
            }
        });

        Button btnJoinEvent = (Button) rootView.findViewById(R.id.detail_join_button);
        btnJoinEvent.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View view) {
                joinEvent();
            }
        });

        Button btnOffers = (Button) rootView.findViewById(R.id.detail_ofertas_button);
        btnOffers.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), OffersActivity.class);
                intent.setData(mEventUri);
                startActivityForResult(intent,1);
            }
        });

        // NETWORK

        mNetworkReceiver = new NetworkReceiver(new NetworkHelper(){
            @Override
            public void disconnected() {
                Toast.makeText(
                        getActivity(),
                        R.string.connection_lost,
                        Toast.LENGTH_LONG).show();
                mConnection = false;
            }

            @Override
            public void connected() {
                mConnection = true;
            }
        });

        mNetworkFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        getActivity().registerReceiver(mNetworkReceiver,mNetworkFilter);

        // Get attendees for this event
        loadInvitees();

        return rootView;
    }

    private void loadInvitees(){

        if(!mConnection){
            Toast.makeText(
                    getActivity(),
                    R.string.connection_lost,
                    Toast.LENGTH_SHORT).show();
            return;
        }

        new RestHelper(
                getActivity(),
                RestHelper.PATH_USER_EVENT,
                ContentUris.parseId(mEventUri)
        ).execute();
    }

    private void listInvitees(){
        // Get users invited to this event
        long id = ContentUris.parseId(mEventUri);

        // Start invitees activity
        Uri uri = EventContract.UserEventEntry.buildUserEventUri(id);
        Intent intent = new Intent(getActivity(),InviteesActivity.class).setData(uri);
        startActivityForResult(intent,1);
    }

    private void joinEvent(){
        if(mEventUri != null) {
            if (!mConnection) {
                Toast.makeText(
                        getActivity(),
                        R.string.connection_lost,
                        Toast.LENGTH_SHORT).show();
                return;
            }

            new PostHelper(
                    getActivity(),
                    RestHelper.PATH_USER_EVENT,
                    mEventUri
            ).execute();

            Intent intent = getActivity().getIntent();
            getActivity().finish();
            startActivity(intent);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.detailfragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_logout: {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
                getActivity().finish();
                return true;
            }
            case R.id.action_settings: {
                if(!mConnection) {
                    Toast.makeText(getActivity(),
                            getString(R.string.connection_error),Toast.LENGTH_SHORT).show();
                    return true;
                }
                Intent intent = new Intent(getActivity(), ProfileActivity.class);
                startActivity(intent);
                return true;
            }
            case R.id.action_location: {
                openGeoLocation();
                return true;
            }
            default: return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        getLoaderManager().initLoader(DETAIL_LOADER, null, this);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if(mEventUri != null){
            return new CursorLoader(
                    getActivity(),
                    mEventUri,
                    EVENT_COLUMNS,
                    null,
                    null,
                    null
            );
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        if(data != null && data.moveToFirst()){
            mLongitude = data.getString(COL_EVENT_LONG);
            mLatitude = data.getString(COL_EVENT_LAT);

            mEventName = data.getString(COL_EVENT_NAME);
            mNameView.setText(mEventName);

            long date = data.getLong(COL_EVENT_DATE);
            mDateView.setText(Utility.getFriendlyDayString(getActivity(),date));

            String desc = data.getString(COL_EVENT_DESC);
            mDetailView.setText(desc);

            mTimeView.setText(Utility.formatTime(date));

        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {}


    private void openGeoLocation(){
        String location = mLatitude+","+mLongitude+"("+mEventName+")";

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("geo:0,0?").buildUpon()
                .appendQueryParameter("q",location).build()
        );

        if(intent.resolveActivity(getActivity().getPackageManager())!=null){
            startActivity(intent);
        } else {
            Toast.makeText(getActivity(),"Nao foi possivel abrir o mapa",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(mNetworkReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(mNetworkReceiver, mNetworkFilter);
    }

}