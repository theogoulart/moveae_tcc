package br.com.sblack.myapp.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import br.com.sblack.myapp.EventFragment;
import br.com.sblack.myapp.InviteesFragment;
import br.com.sblack.myapp.OfferPaymentFragment;
import br.com.sblack.myapp.R;


public class OfferPaymentActivity extends ActionBarActivity implements OfferPaymentFragment.initPayment{

    private static final String CONFIG_CLIENT_ID = "ARfFJh_D1k7tiNeaSm8VEB8IV-igrNflQNjkBThTZQGCLKllXDdG1zuyIviWiXpx1dDsivLwJI-EgrXG";
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;

    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;

    private static final String OFFERPAYMENTFRAGMENT_TAG = "opftag";

    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID)
            // the following are only used in PayPalFuturePaymentActivity.
            .merchantName("Move Ae Store")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));

    private Uri mOfferUri;

    public OfferPaymentActivity(){}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setElevation(0);
        setContentView(R.layout.activity_payment);

        if(getIntent() != null){
            mOfferUri = getIntent().getData();
        }

        Intent paypalIntent = new Intent(this, PayPalService.class);
        paypalIntent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(paypalIntent);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new OfferPaymentFragment(), OFFERPAYMENTFRAGMENT_TAG)
                    .commit();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm = data
                        .getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        System.out.println(confirm.toJSONObject().toString(4));
                        System.out.println(confirm.getPayment().toJSONObject()
                                .toString(4));
                        Toast.makeText(this, "Obrigado! Um e-mail de confirmacao sera enviado no prazo de duas hora",
                                Toast.LENGTH_LONG).show();
                        new PurchasePoster(confirm.toJSONObject().toString(4),this).execute();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                System.out.println("The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                System.out
                        .println("An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        } else if (requestCode == REQUEST_CODE_FUTURE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth = data
                        .getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("FuturePaymentExample", auth.toJSONObject()
                                .toString(4));
                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("FuturePaymentExample", authorization_code);
                        sendAuthorizationToServer(auth);
                        Toast.makeText(this,
                                "Future Payment code received from PayPal",
                                Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        Log.e("FuturePaymentExample",
                                "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("FuturePaymentExample", "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("FuturePaymentExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        }
    }

    @Override
    public void pay(PayPalPayment thingToBuy) {
        Intent intent = new Intent(this,
                PaymentActivity.class);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    private void sendAuthorizationToServer(PayPalAuthorization authorization) {
    }

    @Override
    public void onBackPressed() {
        OfferPaymentFragment fragment = (OfferPaymentFragment)
                getSupportFragmentManager().findFragmentByTag(OFFERPAYMENTFRAGMENT_TAG);
        fragment.onBackPressed();
    }

    @Override
    public void onDestroy() {
        // Stop service when done
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }


    public class PurchasePoster extends AsyncTask<Void, Void, Boolean> {

        private final String LOG_TAG = PurchasePoster.class.getSimpleName();

        private Context mContext;
        private String mResultStr;
        private long mOfferId;
        private String mReceipt;

        PurchasePoster(String receipt,Context context) {
            this.mOfferId = ContentUris.parseId(mOfferUri);
            this.mReceipt = receipt;
            mContext = context;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            String URL = "http://moveae.esy.es/usercupom";

            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;
            InputStream inputStream;
            final String AUTH_KEY = PreferenceManager
                    .getDefaultSharedPreferences(mContext).getString(LoginActivity.AUTH_KEY,null);

            try{

                JSONObject object = new JSONObject(mReceipt);
                object = object.getJSONObject("response");
                mReceipt = object.getString("id");

                java.net.URL url = new URL(URL);

                // Connect
                {
                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setRequestProperty("Authorization", AUTH_KEY);
                    urlConnection.setConnectTimeout(10000);
                    urlConnection.setDoInput(true);
                    urlConnection.setDoOutput(true);
                }

                String output = "cupom_id=" + URLEncoder.encode(Long.toString(mOfferId), "UTF-8") +
                        "&cupom_receipt=" + URLEncoder.encode(mReceipt, "UTF-8");

                // Query
                {
                    DataOutputStream outputStream =
                            new DataOutputStream(urlConnection.getOutputStream());
                    outputStream.writeBytes(output);
                    outputStream.flush();
                    outputStream.close();

                    int response = urlConnection.getResponseCode();

                    Log.d(LOG_TAG,"The response is: " + response);
                    if(response != 200) {
                        return false;
                    }

                    inputStream = new BufferedInputStream(urlConnection.getInputStream());
                    JSONObject jsonObject = new JSONObject(inputStreamToString(inputStream));

                    if(jsonObject.has("error")){
                        mResultStr = jsonObject.getString("error");
                        return true;
                    }
                    mResultStr = jsonObject.getJSONObject("data").getString(":cupom_code");
                }

            } catch(MalformedURLException e){
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }catch(JSONException e){
                e.printStackTrace();
            } finally {
                if(urlConnection != null){
                    urlConnection.disconnect();
                }
                if(reader != null){
                    try{
                        reader.close();
                    } catch (IOException e){
                        e.printStackTrace();
                    }
                }
            }

            return true;
        }

        private String inputStreamToString(InputStream inputStream) throws IOException, UnsupportedEncodingException {

            BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
            String line = "";
            String result = "";
            while((line = bufferedReader.readLine()) != null)
                result += line;
            inputStream.close();
            return result;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            if (success) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setMessage("Obrigado pela compra! \n \nAnote o codigo do seu cupom e apresente na loja para retirar a oferta\n---------------" + mResultStr+"---------------")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {}
                        });
                AlertDialog alert = builder.create();
                alert.show();
            } else{
                Toast.makeText(mContext,mResultStr,Toast.LENGTH_LONG).show();
            }
        }
    }

}