package br.com.sblack.myapp.network;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.Vector;

import br.com.sblack.myapp.R;
import br.com.sblack.myapp.activities.LoginActivity;
import br.com.sblack.myapp.data.EventContract;

public class RestHelper extends AsyncTask<String,Void,String> {

    private static final String LOG_TAG = RestHelper.class.getSimpleName();

    public static final String PATH_EVENTS = "events";
    public static final String PATH_FIXED = "fixed";
    public static final String PATH_USER_EVENT = "userevent";
    public static final String SEARCH_PARAM = "search";
    private static final String ERROR_KEY = "error";

    private Context mContext;
    private String mMethod;
    private long mId;

    public RestHelper(Context context, String method){
        this.mContext = context;
        this.mMethod = method;
        this.mId = -1;
    }

    public RestHelper(Context context, String method, long id){
        this.mContext = context;
        this.mMethod = method;
        this.mId = id;
    }

    @Override
    protected void onPostExecute(String result) {
        if(result != null) {
            Toast.makeText(mContext, result, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCancelled() {
        Toast.makeText(mContext,mContext.getString(R.string.connection_error),Toast.LENGTH_SHORT).show();
    }

    @Override
    protected String doInBackground(String... params) {

        Uri builtUri;
        String result = null;
        String error;

        try {
            final String BASE_URL = "http://moveae.esy.es";

            // http://moveae.esy.es/<METHOD>
            builtUri = Uri.parse(BASE_URL).buildUpon().appendPath(mMethod).build();

            if(params.length != 0){
                builtUri = builtUri.buildUpon().appendQueryParameter(SEARCH_PARAM,params[0]).build();
            }
            if(mId != -1){
                // http://moveae.esy.es/<METHOD>/<ID>
                builtUri = builtUri.buildUpon().appendPath(Long.toString(mId)).build();
            }

            result = fetchData(builtUri);

            if(result != null) {
                error = checkError(result);
                if(error != null) return error;

                if (mMethod.equals(PATH_EVENTS) || mMethod.equals(PATH_FIXED)) {
                    insertEventFromJSON(result);
                }
                deleteOldData();
                if (mMethod.equals(PATH_USER_EVENT)) {
                    insertUserFromJSON(result);
                }
            }
        } catch(JSONException e){
            e.printStackTrace();
            return null;
        } catch(IndexOutOfBoundsException e){
            e.printStackTrace();
            return null;
        }

        return null;
    }

    private String checkError(String result){
        try {
            JSONObject jsonObject = new JSONObject(result);
            if(jsonObject.has(ERROR_KEY)) {
                return jsonObject.getString(ERROR_KEY);
            }

        } catch (JSONException e){
            e.printStackTrace();
        }
        return null;
    }

    private String fetchData(Uri builtUri){

        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        InputStream inputStream;
        final String AUTH_KEY = PreferenceManager
                .getDefaultSharedPreferences(mContext).getString(LoginActivity.AUTH_KEY,null);

        String resultStr = null;

        try{
            URL url = new URL(builtUri.toString());

            // Connect
            {
                urlConnection = (HttpURLConnection) url.openConnection();
                // Append Authorization header to the request
                urlConnection.setRequestProperty("Authorization", AUTH_KEY);
                urlConnection.setRequestMethod("GET");
                urlConnection.setConnectTimeout(10000);
                urlConnection.setDoInput(true);
                urlConnection.connect();
            }

            // Query
            {
                int response = urlConnection.getResponseCode();


                if(response != 200) {
                    Log.d(LOG_TAG, "The response is: " + response);
                    return null;
                }

                inputStream = new BufferedInputStream(urlConnection.getInputStream());
            }

            resultStr = inputStreamToString(inputStream);

        } catch(MalformedURLException e){
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        } finally {
            if(urlConnection != null){
                urlConnection.disconnect();
            }
            if(reader != null){
                try{
                    reader.close();
                } catch (IOException e){
                    e.printStackTrace();
                }
            }
        }

        return resultStr;
    }

    private void insertUserFromJSON(String resultJson) throws JSONException{
        //JSON OBJECT names to get from JSON
        final String JO_NAME = "user_name";
        final String JO_EMAIL = "user_email";
        final String JO_ID = "user_id";

        Vector<ContentValues> cvVector = new Vector<>();
        Vector<ContentValues> cvUEVector = new Vector<>();

        try{
            // Get JSON params from object
            {
                JSONObject jsonEvents = new JSONObject(resultJson);
                JSONArray jsonArray = jsonEvents.getJSONArray("data");

                long eventid = jsonEvents.getLong("event_id");

                for(int i = 0; i<jsonArray.length() ; i++) {
                    String name;
                    String email;
                    long id;

                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    name = jsonObject.getString(JO_NAME);
                    email = jsonObject.getString(JO_EMAIL);
                    id = jsonObject.getLong(JO_ID);

                    // Add content values for USER table
                    {
                        ContentValues contentValues = new ContentValues();

                        contentValues.put(EventContract.UserEntry.COLUMN_USER_ID, id);
                        contentValues.put(EventContract.UserEntry.COLUMN_NAME, name);
                        contentValues.put(EventContract.UserEntry.COLUMN_EMAIL, email);

                        cvVector.add(contentValues);
                    }

                    // Add content values for USER_EVENT table
                    {
                        if(addUserEvent(id, eventid)) {

                            ContentValues uECValues = new ContentValues();

                            uECValues.put(EventContract.UserEventEntry.COLUMN_KEY_EVENT, eventid);
                            uECValues.put(EventContract.UserEventEntry.COLUMN_KEY_USER, id);

                            cvUEVector.add(uECValues);
                        }
                    }
                }

            }

            // insert into database
            {
                int rowsInserted = 0;

                if(cvVector.size() > 0) {
                    // First insert all the users returned by the API
                    ContentValues[] cvArray = new ContentValues[cvVector.size()];
                    cvVector.toArray(cvArray);
                    rowsInserted = mContext.getContentResolver().bulkInsert(
                            EventContract.UserEntry.CONTENT_URI, cvArray);
                }

                if(cvUEVector.size() > 0){
                    // Then insert all USER_EVENT records for users attending this event
                    ContentValues[] cvArray = new ContentValues[cvVector.size()];
                    cvUEVector.toArray(cvArray);
                    mContext.getContentResolver().bulkInsert(
                            EventContract.UserEventEntry.CONTENT_URI, cvArray
                    );
                }

                Log.d(LOG_TAG,"Fetch Users Complete: "+ rowsInserted + " rows inserted");
            }

        } catch(JSONException e){
            e.printStackTrace();
        } catch(SQLException e){
            e.printStackTrace();
        }
    }

    private void insertEventFromJSON(String resultJson) throws JSONException{

        //JSON OBJECT names to get from JSON
        final String JO_EVENT_ID = "event_id";
        final String JO_NAME = "event_name";
        final String JO_INTRO = "event_intro";
        final String JO_DESC = "event_description";
        final String JO_DATE = "event_date";
        final String JO_USER_ID = "user_id";
        final String JO_CAT_ID = "category_id";
        final String JO_LAT = "event_lat";
        final String JO_LONG = "event_long";
        final String JO_MODEL = "event_model";

        Vector<ContentValues> cvVector = new Vector<>();

        try{
            // Get JSON params from object
            {
                JSONObject jsonEvents = new JSONObject(resultJson);
                JSONArray jsonArray = jsonEvents.getJSONArray("data");

                for(int i = 0; i<jsonArray.length() ; i++){
                    String eventId;
                    String name;
                    String intro;
                    String desc;
                    String icon;
                    String categoryId;
                    String userId;
                    String date;
                    String lat;
                    String lon;
                    String model;

                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    eventId = jsonObject.getString(JO_EVENT_ID);
                    name = jsonObject.getString(JO_NAME);
                    intro = jsonObject.getString(JO_INTRO);
                    desc = jsonObject.getString(JO_DESC);
                    date = jsonObject.getString(JO_DATE);
                    userId = jsonObject.getString(JO_USER_ID);
                    categoryId = jsonObject.getString(JO_CAT_ID);
                    lat = jsonObject.getString(JO_LAT);
                    lon = jsonObject.getString(JO_LONG);
                    model = jsonObject.getString(JO_MODEL);
                    icon = "madeup";

                    ContentValues contentValues = new ContentValues();

                    contentValues.put(EventContract.EventEntry.COLUMN_EVENT_NAME,name);
                    contentValues.put(EventContract.EventEntry.COLUMN_EVENT_ID,eventId);
                    contentValues.put(EventContract.EventEntry.COLUMN_EVENT_INTRO,intro);
                    contentValues.put(EventContract.EventEntry.COLUMN_EVENT_DESC,desc);
                    contentValues.put(EventContract.EventEntry.COLUMN_EVENT_DATE,date);
                    contentValues.put(EventContract.EventEntry.COLUMN_USER_KEY,userId);
                    contentValues.put(EventContract.EventEntry.COLUMN_CAT_KEY,categoryId);
                    contentValues.put(EventContract.EventEntry.COLUMN_EVENT_LAT,lat);
                    contentValues.put(EventContract.EventEntry.COLUMN_EVENT_LONG,lon);
                    contentValues.put(EventContract.EventEntry.COLUMN_EVENT_MODEL,model);
                    contentValues.put(EventContract.EventEntry.COLUMN_EVENT_ICON,icon);

                    cvVector.add(contentValues);
                }

            }

            // insert into database
            {
                int rowsInserted = 0;
                // insert events into database
                if(cvVector.size() > 0){
                    ContentValues[] cvArray = new ContentValues[cvVector.size()];
                    cvVector.toArray(cvArray);
                    rowsInserted = mContext.getContentResolver().bulkInsert(
                            EventContract.EventEntry.CONTENT_URI, cvArray);
                }

                Log.d(LOG_TAG,"Fetch Events Complete: "+ rowsInserted + " rows inserted");
            }

        } catch(JSONException e){
            e.printStackTrace();
        }
    }

    private String inputStreamToString(InputStream inputStream) throws IOException, UnsupportedEncodingException {

        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;
        inputStream.close();
        return result;
    }

    private void deleteOldData(){
        String time = Long.toString(System.currentTimeMillis()-30*60*1000);

        if(time != null){
            mContext.getContentResolver().delete(
                    EventContract.EventEntry.CONTENT_URI,
                    EventContract.EventEntry.COLUMN_EVENT_DATE + " <= "+ time,
                    null
            );
        }
    }

    private boolean addUserEvent(long userId, long eventId){

        String selection = EventContract.UserEventEntry.COLUMN_KEY_EVENT + "= ? AND " +
                EventContract.UserEventEntry.COLUMN_KEY_USER + " = ?";

        Cursor cursor = mContext.getContentResolver().query(
                EventContract.UserEventEntry.CONTENT_URI,
                new String[]{EventContract.UserEventEntry.COLUMN_KEY_USER},
                selection,
                new String[]{Long.toString(eventId), Long.toString(userId)},
                null
        );

        if(cursor.getCount() == 0) {
            return true;
        }
        return false;
    }

}

