<?php

require_once('Database.php');

Class ModelEvent extends Database{

	public function addEvent($event_data){
		return Database::submit_query('INSERT INTO events (`event_name`, `event_intro`, `event_description`, `event_date`, `user_id`, `category_id`, `event_long`, `event_lat`, `event_model`) VALUES (:event_name, :event_intro, :event_description, :event_date, :user_id, :category_id, :event_long, :event_lat, :event_model)', $event_data);
	}

	public function listEvent(){
		//TODO listar apenas eventos ativos
		return Database::get_query('SELECT * FROM events WHERE event_model = 1 LIMIT 30');
	}

	public function searchEvent($search){
		//TODO listar apenas eventos ativos
		return Database::get_query('SELECT * FROM events WHERE event_name like :event_name AND event_model = 1 ',
			[':event_name' => '%'.$search.'%']);
	}

	public function getEvent($event_id){
		return Database::get_query('SELECT * FROM events WHERE event_id like :event_id AND event_model = 1',
			[':event_id' =>  $event_id]);
	}

	public function joinEvent($event_data){
		return Database::submit_query('INSERT INTO user_event (`user_id`,`event_id`) VALUES (:user_id,:event_id)', $event_data);
	}

	// verify in how many events a user is registred
	public function verifyIfUserAreRegisteredInOtherEvent($event_check_user){
		$resultSet =Database::get_query('
			SELECT *
			FROM events 
			WHERE user_id = :user_id and event_date = :event_date', $event_check_user);
	}

	public function getUserByLogin($user_data){
		return Database::get_query('
		SELECT * 
		FROM users 

		
		WHERE user_email = :user_email and user_password = :user_password', $user_data);
	}


	// VALIDATE EVENT NAME


	// verify if event name already exists
	public function EventNameExists($event_name){
		$result = false;		
		$resultSet = Database::get_query('
			SELECT * FROM events 
			WHERE event_name = :event_name;', [':event_name' => $event_name]);
		if(!empty($resultSet)){
			$result = true;
		}
		return $result;
	}

	// verify if event name is empty
	public function EventNameEmpty($event_name){
		$result = false;
		if(empty($event_name)){
			$result = true;
		}
		return $result;
	}

	// verify the size of event name is valid
	public function EventNameLength($event_name){
		$result = false;
		if(strlen($event_name) > 128){
			$result = true;
		}
		return $result;
	}

	// verify if event name is valid
	public function verifyFillEventName($event_name){
		$result = false;
		if (preg_match('/[\'£$%&*()}{@#><>|=+¬]/', $event_name)){
	    	$result = true;
		}
		//verify if all characteres in event name is blank spaces
		if (ctype_space($event_name)){
	    	$result = true;
		}
		return $result;
	}

	// remove extra blank spaces of event name
	public function removeBlankSpacesEventName($event_name){
		//$text = str_replace(" ", "", $user_name);
		trim($event_name);
		$str = preg_replace('/ {2,}/',' ',$event_name);
		return $str;
	}


	// VALIDATE EVENT INTRO


	//verify if event intro is valid
	public function verifyFillEventIntro($event_intro){
		$result = false;
		if (preg_match('/[\'^£$%&*()}{@#~?><>|=+¬]/', $event_intro)){
	    	$result = true;
		}
		//verify if all characteres in event intro is blank spaces
		if (ctype_space($event_intro)){
	    	$result = true;
		}
		return $result;	
	}

	// verify if size of event intro is valid
	public function EventIntroLength($event_intro){
		$result = false;		
		if(strlen($event_intro) > 256){
			$result = true;
		}
		return $result;
	}

	// verify if event intro is empty
	public function EventIntroEmpty($event_intro){
		$result = false;		
		if(empty($event_intro)){
			$result = true;
		}
		return $result;
	}

	// verify if event intro already exists
	public function EventIntroExists($event_intro){
		$result = false;		
		$resultSet = Database::get_query('
			SELECT * FROM events 
			WHERE event_intro = :event_intro;', [':event_intro' => $event_intro]);
		if(!empty($resultSet)){
			$result = true;
		}
		return $result;
	}

	public function removeBlankSpacesEventIntro($event_intro){
		//$text = str_replace(" ", "", $event_intro);
		trim($event_intro);
		$str = preg_replace('/ {2,}/',' ',$event_intro);
		return $str;
	}


	// VALIDATE EVENT DESCRIPITION


	public function verifyFillEventDesc($event_description){
		$result = false;
		if (preg_match('/[\'^£$%&*()}{@#~?><>|=+¬]/', $event_description)){
	    	$result = true;
		}
		//verify if all characteres in event descripition is blank spaces
		if (ctype_space($event_description)){
	    	$result = true;
		}
		return $result;	
	}

	public function EventDescLength($event_description){
		$result = false;		
		if(strlen($event_description) > 1000){
			$result = true;
		}
		return $result;
	}

	public function EventDescEmpty($event_description){
		$result = false;		
		if(empty($event_description)){
			$result = true;
		}
		return $result;
	}

	//serviço verifica se nome do evento já existe
	public function EventDescExists($event_description){
		$result = false;		
		$resultSet = Database::get_query('
			SELECT * FROM events 
			WHERE event_description = :event_description;', [':event_description' => $event_description]);
		if(!empty($resultSet)){
			$result = true;
		}
		return $result;
	}

	public function removeBlankSpacesEventDesc($event_description){		
		//$text = str_replace(" ", "", $event_description);
		trim($event_description);
		$str = preg_replace('/ {2,}/',' ',$event_description);
		return $str;
	}


	// VALIDATE DATA


	// verify if data is a valid one
	public function validateDate($date, $format = 'Y-m-d H:i:s'){
		
		$parts = explode('-', $date);

		if($parts[0] < 1970 or $parts[0] > 2038){
			return false;
		}

    	$d = DateTime::createFromFormat($format, $date);

    	return $d && $d->format($format) == $date;
    }

}