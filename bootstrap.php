<?php 

function pr($v){
	echo '<pre>';
	print_r($v);
}

function error_404() {
	api_response(['error' => 'page_not_found'], 404);
	return;
}

function cleanUrl($url) {
	$url = trim($url, '/');
	$url = explode('?', $url);
	$url = $url[0];
	$url = explode('/', $url);
	return $url;
}

function instantiate($file) {

	if (isset($GLOBALS[$file])) {
		return $GLOBALS[$file];
	}

	if (file_exists('../app/'.$file.'.php')) {
		require('../app/'.$file.'.php');
		$path = cleanUrl($file);
		$GLOBALS[$file] = new $path[1]();
		return $GLOBALS[$file];
	}
	return false;
}

function escape($string = '') {
	// $string = addslashes($string);
	$string = htmlspecialchars($string, ENT_NOQUOTES);
	$string = str_replace('&amp;', '&', $string);
	return $string;
}

function _post($name = false, $orReturn = false){
	if ($name)
		$name = (array_key_exists($name, $_POST) && $_POST[$name] !== "") ? escape($_POST[$name]) : $orReturn;
	return $name;
}

function _get($name = false, $orReturn = false){
	if ($name)
		$name = (array_key_exists($name, $_GET) && $_GET[$name] !== "") ? escape($_GET[$name]) : $orReturn;
	return $name;	
}

function api_response($data, $status = 200) {
	header('Content-type: text/json');
	http_response_code($status);
	echo json_encode($data);
}

function includeView($file, $path, $data = false) {
	include('../app/View/'.$path.'/'.$file.'.php');
	if ($data) {
		extract($data, EXTR_PREFIX_SAME, "var");
	}
	
}

function bootstrap($url) {
	$url = cleanUrl($url);

	$controllerName = ucfirst(((!empty($url[0])) ? $url[0] : 'Home') . 'Controller');
	$actionFallback = ((isset($url[1])) ? $url[1] : 'index');
	$actionFallback = (is_numeric($actionFallback) ? 'id' : $actionFallback);
	$actionName 	= $actionFallback.$_SERVER['REQUEST_METHOD'];
	$param 			= ($actionFallback == 'id') ? $url[1] : null;

	$controller = instantiate('controller'.'/'.$controllerName);

	if ((! $controller) || ((! method_exists($controller, $actionName)) && (! method_exists($controller, $actionFallback)))){
		return error_404();
	}

	if (method_exists($controller, $actionName)) {
		return $controller->{$actionName}($param);
	} else {
		return $controller->{$actionFallback}($param);
	}
	
}
