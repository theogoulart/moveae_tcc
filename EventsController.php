<?php 

Class EventsController {

	public function __construct() {
		instantiate('Helper/Auth')->check();
	}

	// Listar eventos com filtro
	public function indexGET() {

		$search = _get('search', false);

		if ($search) {
			$event_list = instantiate('Model/ModelEvent')->searchEvent($search);
		} else {
			$event_list = instantiate('Model/ModelEvent')->listEvent();
		}
		
		return api_response(['success'=>true,'data'=>$event_list],200);
	}

	// Criar evento
	public function indexPOST() {

		//TODO validar os dados inseridos
		$event_name 		= _post('event_name'); 
		$event_intro 		= _post('event_intro');
		$event_description 	= _post('event_description');
		$event_date 		= _post('event_date'); 
		$user_id 			= instantiate('Helper/Auth')->getId();
		$category_id 		= _post('category_id');
		$event_long 		= _post('event_long');
		$event_lat 			= _post('event_lat');
		$event_model 		= _post('event_model');

		// REMOVE BLANK SPACES OF FIELDS POST
		$event_name = instantiate('Model/ModelEvent')->removeBlankSpacesEventName($event_name);

		$event_intro = instantiate('Model/ModelEvent')->removeBlankSpacesEventIntro($event_intro);

		$event_description = instantiate('Model/ModelEvent')->removeBlankSpacesEventDesc($event_description);

		$event_data = [
			':event_name'		=>$event_name, 
			':event_intro'		=>$event_intro, 
			':event_description'=>$event_description,
			':event_date'		=>$event_date, 
			':user_id'			=>$user_id, 
			':category_id'		=>$category_id, 
			':event_long'		=>$event_long, 
			':event_lat'		=>$event_lat, 
			':event_model'		=>$event_model
		];

		$event_check_user = [
			':user_id'			=>$user_id,
			':event_date'		=>$event_date
		];


		//VALIDATE EVENT NAME

		//func verify if event name is empty
		$EventNameEmpty = instantiate('Model/ModelEvent')->EventNameEmpty($event_name);
		if($EventNameEmpty == true){
			return  api_response(['error' => 'Erro: Nome do evento esta vazio']);
		} 

		//func verify the length of event name
		$EventNameLength = instantiate('Model/ModelEvent')->EventNameLength($event_name);
		if($EventNameLength == true){
			return  api_response(['error' => 'Erro: nome do evento e muito longo']);
		}

		// VALIDATE EVENT INTRO

				//func verify the size of field event intro name
		$EventIntroLength = instantiate('Model/ModelEvent')->EventIntroLength($event_intro);
		if($EventIntroLength == true){
			return  api_response(['error' => 'Erro: intro do evento e muito longa']);
		} 

		//func verify if the field event intro name is empty
		$EventIntroEmpty = instantiate('Model/ModelEvent')->EventIntroEmpty($event_intro);
		if($EventIntroEmpty == true){
			return  api_response(['error' => 'Erro: intro do evento esta vazia']);
		} 

		//VALIDATE EVENT DESCRIPITON


		//func verify the length of the field event descripition
		$EventDescLength = instantiate('Model/ModelEvent')->EventDescLength($event_description);
		if($EventDescLength == true){
			return  api_response(['error' => 'Erro: descricao do evento e muito longa']);
		}

		//func verify the field event descripition is empty
		$EventDescEmpty = instantiate('Model/ModelEvent')->EventDescEmpty($event_description);
		if($EventDescEmpty == true){
			return  api_response(['error' => 'Erro: descricao do evento esta vazia']);
		}


		// VALIDATE EVENT DATA

		$validateDate = instantiate('Model/ModelEvent')->validateDate($event_date);
		if($validateDate == false){
			return  api_response(['error' => 'Erro: data invalida']);
		}


		// VALIDATE USER - EVENT


		$event_id = instantiate('Model/ModelEvent')->addEvent($event_data);

		if(!$event_id){
			return api_response(['error'=>'event_error'],500);
		}

		$response = [
			'user_id' => $user_id,
			'event_id' => $event_id
		];

		return api_response(['success'=>true,'data'=>$response,], 200);
	}

	// Obter um evento
	public function idGET($id) {
		$user_id 	= instantiate('Helper/Auth')->getId();
		$event 		= instantiate('Model/ModelEvent')->getEvent($id);
		$members 	= instantiate('Model/ModelUserEvent')->getByEvent($id);
		$organizer 	= instantiate('Model/ModelUser')->getUser($event[0]->user_id);

		return api_response([
			'success'	=> true, 
			'organizer' => $organizer,
			'members' 	=> $members,
			'event' 	=> $event
		]);	
	}
}