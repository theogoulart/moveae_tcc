package br.com.sblack.myapp;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import br.com.sblack.myapp.activities.DetailActivity;
import br.com.sblack.myapp.activities.CreateActivity;
import br.com.sblack.myapp.activities.LoginActivity;
import br.com.sblack.myapp.activities.ProfileActivity;
import br.com.sblack.myapp.data.EventContract;
import br.com.sblack.myapp.network.NetworkHelper;
import br.com.sblack.myapp.network.NetworkReceiver;
import br.com.sblack.myapp.network.RestHelper;

public class EventFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    public static final String LOG_TAG = EventFragment.class.getSimpleName();
    public static final String CONNECTION_EXTRA = "connection";
    private static final String SEARCH_SELECTION = "search_selection";
    private static final int EVENT_LOADER = 0;

    private static final String[] EVENT_COLUMNS = {
            EventContract.EventEntry.TABLE_NAME+"."+ EventContract.EventEntry._ID,
            EventContract.EventEntry.TABLE_NAME+"."+ EventContract.EventEntry.COLUMN_EVENT_ID,
            EventContract.EventEntry.COLUMN_EVENT_NAME,
            EventContract.EventEntry.COLUMN_EVENT_INTRO,
            EventContract.EventEntry.COLUMN_EVENT_DESC,
            EventContract.EventEntry.COLUMN_EVENT_DATE,
            EventContract.EventEntry.COLUMN_USER_KEY,
            EventContract.EventEntry.COLUMN_CAT_KEY,
            EventContract.EventEntry.COLUMN_EVENT_ICON,
            EventContract.EventEntry.COLUMN_EVENT_LAT,
            EventContract.EventEntry.COLUMN_EVENT_LONG,
            EventContract.EventEntry.COLUMN_EVENT_MODEL
    };

    // indices for getting the columns in the event table
    static final int COL_ID = 0;
    static final int COL_EVENT_ID = 1;
    static final int COL_EVENT_NAME = 2;
    static final int COL_EVENT_INTRO = 3;
    static final int COL_EVENT_DESC = 4;
    static final int COL_EVENT_DATE = 5;
    static final int COL_USER_KEY = 6;
    static final int COL_CAT_KEY = 7;
    static final int COL_EVENT_ICON = 8;
    static final int COL_EVENT_LAT = 9;
    static final int COL_EVENT_LONG = 10;
    static final int COL_EVENT_MODEL = 11;

    // Network
    private NetworkReceiver mNetworkReceiver;
    private IntentFilter mNetworkFilter;
    private boolean mConnection;
    private RestHelper mRestHelper;

    // Views
    private EventAdapter mEventAdapter;
    private ListView mListView;
    private Button mBtnSearch;
    private EditText mSearchText;

    public EventFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.forecastfragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch(id) {
            case R.id.action_refresh:{
                updateEvent();
                return true;
            }
            case R.id.action_logout: {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
                getActivity().finish();
                return true;
            }
            case R.id.action_settings: {
                if(!mConnection) {
                    Toast.makeText(getActivity(),
                            getString(R.string.connection_error),Toast.LENGTH_SHORT).show();
                    return true;
                }
                Intent intent = new Intent(getActivity(), ProfileActivity.class);
                startActivity(intent);
                return true;
            }
            default: return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        mEventAdapter = new EventAdapter(getActivity(),null,0);

        // Get a reference to the ListView, and attach this adapter to it.
        mListView = (ListView) rootView.findViewById(R.id.listview_forecast);
        mListView.setAdapter(mEventAdapter);

        // LISTENERS

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                Cursor cursor = (Cursor) adapterView.getItemAtPosition(pos);
                if(cursor != null) {
                    onListItemClicked(cursor);
                }
            }
        });

        mSearchText = (EditText)rootView.findViewById(R.id.search_edittext);

        mBtnSearch = (Button) rootView.findViewById(R.id.search_event_button);
        mBtnSearch.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View view) {
                searchEvent(mSearchText.getText().toString());
                Utility.hideSoftKeyboard(getActivity());
            }
        });

        mSearchText.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    mBtnSearch.performClick();
                    return true;
                }
                return false;
            }
        });

        Button btnCreateEvent = (Button)rootView.findViewById(R.id.button_add_event);
        btnCreateEvent.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View view) {
                onCreateClicked();
            }
        });

        // NETWORK CONNECTION

        mNetworkReceiver = new NetworkReceiver(new NetworkHelper() {
            @Override
            public void disconnected() {
                Toast.makeText(getActivity(), R.string.connection_lost, Toast.LENGTH_LONG).show();
                mConnection = false;
            }
            @Override
            public void connected() {
                mConnection = true;
            }
        });
        mNetworkFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        getActivity().registerReceiver(mNetworkReceiver,mNetworkFilter);

        return rootView;
    }

    private void onCreateClicked(){
        if (!mConnection) {
            Toast.makeText(getActivity(), R.string.connection_lost, Toast.LENGTH_SHORT).show();
            return;
        }

        mRestHelper = new RestHelper(
                getActivity(),
                RestHelper.PATH_FIXED
        );
        mRestHelper.execute();

        Intent intent = new Intent(getActivity(), CreateActivity.class);
        startActivity(intent);
    }

    private void onListItemClicked(Cursor cursor){
        long id = cursor.getLong(COL_EVENT_ID);
        Uri uri = EventContract.EventEntry.buildEventUri(id);

        Intent intent = new Intent(getActivity(), DetailActivity.class).setData(uri);
        intent.putExtra(CONNECTION_EXTRA, mConnection);
        startActivity(intent);
    }

    private void searchEvent(String input){
        if(input != null) {
            if(mConnection) {
                mRestHelper = new RestHelper(
                        getActivity(),
                        RestHelper.PATH_EVENTS
                );
                mRestHelper.execute(input);
            }
            else{
                Toast.makeText(getActivity(),R.string.connection_lost_search,Toast.LENGTH_SHORT).show();
            }

            Bundle bundle = new Bundle();

            String selection = EventContract.EventEntry.COLUMN_EVENT_NAME + " LIKE '%"+ input +"%'";
            bundle.putString(SEARCH_SELECTION, selection);

            getLoaderManager().restartLoader(EVENT_LOADER, bundle, this);
        } else {
            getLoaderManager().restartLoader(EVENT_LOADER,null,null);
        }
    }

    private void updateEvent(){
        if(!mConnection) {
            Toast.makeText(
                    getActivity(),
                    R.string.connection_lost,
                    Toast.LENGTH_SHORT).show();
            return;
        }

        mRestHelper = new RestHelper(
                getActivity(),
                RestHelper.PATH_EVENTS
        );
        mRestHelper.execute();

        getLoaderManager().restartLoader(EVENT_LOADER, null, this);
        Toast.makeText(getActivity(), getString(R.string.toast_refresh), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        getLoaderManager().initLoader(EVENT_LOADER,null,this);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {

        String selection = null;

        if(bundle != null && bundle.containsKey(SEARCH_SELECTION)){
            selection = bundle.getString(SEARCH_SELECTION);
        }

        switch (i) {
            case EVENT_LOADER: {

                Uri uri = EventContract.EventEntry.CONTENT_URI;
                String sortOrder = EventContract.EventEntry.COLUMN_EVENT_DATE + " ASC";

                return new CursorLoader(
                        getActivity(),
                        uri,
                        EVENT_COLUMNS,
                        selection,
                        null,
                        sortOrder);
            }
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        mEventAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        mEventAdapter.swapCursor(null);
    }

    @Override
    public void onPause() {
        super.onPause();
        Utility.sParam = mSearchText.getText().toString();
        getActivity().unregisterReceiver(mNetworkReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        mSearchText.setText(Utility.sParam);
        getActivity().registerReceiver(mNetworkReceiver,mNetworkFilter);
    }

}